import { createWriteStream } from 'fs'
import { dirname, resolve } from 'path'
import { fileURLToPath } from 'url'
import axios from 'axios'
import ffmpeg from 'fluent-ffmpeg'
import installer from '@ffmpeg-installer/ffmpeg'

import { removeFile } from './utils.js'
import { VOICES_FOLDER_NAME } from './constants.js'

const __dirname = dirname(fileURLToPath(import.meta.url))

class OggConverter {
  constructor() {
    ffmpeg.setFfmpegPath(installer.path)
  }

  toMp3(input, output) {
    try {
      const outputPath = resolve(dirname(input), `${output}.mp3`)
      return new Promise((resolve1, reject) => {
        ffmpeg(input)
          .inputOption('-t 30')
          .output(outputPath)
          .on('end', () => {
            removeFile(input)
            resolve(outputPath)
          })
          .on('error', (err) => reject(err.message))
          .run()
      })
    } catch (err) {
      console.error(`Error on converting ogg to mp3`)
    }
  }

  async create(url, filename) {
    try {
      const oggPath = resolve(__dirname, `../${VOICES_FOLDER_NAME}`, `${filename}.ogg`)
      const response = await axios({
        method: 'get',
        url,
        responseType: 'stream'
      })
      return new Promise(resolve => {
        const stream = createWriteStream(oggPath)
        response.data.pipe(stream)
        stream.on('finish', () => resolve(oggPath))
      })
    } catch (err) {
      console.error(`Error on oga file create: ${err.message}`)
    }
  }
}

export const ogg = new OggConverter()