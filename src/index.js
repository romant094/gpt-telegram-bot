import { Telegraf } from 'telegraf'
import config from 'config'
import { stat, mkdir } from 'fs'
import { ogg } from './ogg.js'
import { VOICES_FOLDER_NAME } from './constants.js'

stat(VOICES_FOLDER_NAME, err => {
  if (err) {
    if (err.code === 'ENOENT') {
      mkdir(VOICES_FOLDER_NAME, (err) => {
        if (err) {
          console.error(`Error on create folder '${VOICES_FOLDER_NAME}': ${err}`)
        } else {
          console.log(`Folder '${VOICES_FOLDER_NAME}' created`)
        }
      });
    } else {
      console.error(`Error on check '${VOICES_FOLDER_NAME}' folder`);
    }
  } else {
    console.log(`Folder '${VOICES_FOLDER_NAME}' exists`);
  }
});

const bot = new Telegraf(config.get('telegram_token'))

bot.on('voice', async context => {
  try {
    const link = await context.telegram.getFileLink(context.message.voice.file_id)
    const userId = String(context.message.from.id)
    const oggPath = await ogg.create(link.href, userId)
    const mp3Path = await ogg.toMp3(oggPath, userId)
    context.reply(mp3Path)
  } catch (err) {
    console.error(`Error on voice message: ${err.message}`)
  }
})
bot.command('start', async context => {
  await context.reply(config.get('welcome_message'))
})

bot.launch()

process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))